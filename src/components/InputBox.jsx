import React, { useRef, useEffect, useState } from 'react'
import Items from './Items'

const InputBox = () => {
  const [inputValue, setInputValue] = useState('')
  const [items, setItems] = useState([])
  // const inputRef = useRef(null)

  const handle = () => {
    if (inputValue === '') {
      return alert('Please enter the task')
    }
    setItems(items => {
      return [...items, { txt: inputValue, id: crypto.randomUUID() }]
    })
    setInputValue('')
  }

  // useEffect(() => {
  //   inputRef.current.focus()
  //   }, [])

  const keyhandler = e => {
    if (e.key === 'Enter') {
      handle()
    }
  }

  return (
    <>
      <div className='input-box'>
        <input
          type='text'
          value={inputValue}
          onChange={e => {
            setInputValue(e.target.value)
          }}
          placeholder='enter todo'
          // onBlur={handle} //when you leave out of the input focus
          onKeyDown={keyhandler}
          // ref={inputRef} 
        />
        <button className='btn adder' onClick={handle}>
          Add Task
        </button>
      </div>
      <Items items={items} setItems={setItems} />
    </>
  )
}

export default InputBox
