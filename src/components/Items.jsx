import React, { useState } from 'react'

const Items = ({ items, setItems }) => {
  const [editingItem, setEditingItem] = useState(null)
  const [editText, setEditText] = useState('')

  const delhandle = id => {
    setItems(items.filter(ele => ele.id !== id))
  }

  const editHandle = (id, newText) => {
    setItems(
      items.map(item => {
        console.log(item)
        if (item.id === id) {
          return { ...item, txt: newText }
        }
        return item
      })
    )
    setEditingItem(null)
  }

  const saveData = (id, editText) => {
    editHandle(id, editText)
  }
  return (
    <div className='items'>
      <ul>
        {items.map(ele => {
          return (
            <div className='item'>
              {editingItem === ele.id ? (
                <>
                  <input
                    type='text'
                    value={editText}
                    onChange={e => setEditText(e.target.value)}
                    autoFocus
                    onBlur={() => editHandle(ele.id, editText)}
                    onKeyDown={e => {
                      if (e.key === 'Enter') {
                        editHandle(ele.id, editText)
                      }
                    }}
                  />
                  <button
                    onClick={() => {
                      saveData(ele.id, editText)
                    }}
                  >
                    save
                  </button>
                </>
              ) : (
                <>
                  <li key={ele.id}>{ele.txt}</li>
                  <button
                    onClick={() => {
                      setEditingItem(ele.id)
                      setEditText(ele.txt)
                    }}
                  >
                    Edit
                  </button>
                  <button onClick={() => delhandle(ele.id)}>Del</button>
                </>
              )}
            </div>
          )
        })}
      </ul>
    </div>
  )
}

export default Items
